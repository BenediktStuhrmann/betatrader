package businesslogic;

import data.DAOFactory;
import data.DAOFactory.Backend;
import data.TradeDAO;

public class TradeManager {
	TradeDAO tradeDAO;
	
	public TradeManager() {
		super();
		tradeDAO = DAOFactory.getDAOFactory(Backend.H2).getTradeDAO();
	}
}
